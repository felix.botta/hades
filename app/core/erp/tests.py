from config.wsgi import *
from core.erp.models import Type, Employee


# Listar em SQL
# select * from NomeDaTabela

# query = Type.objects.all()
# print(query)

# Inserir

# t = Type()
# t.name = 'Gestor'
# t.save()

# OU t = Type(name='Gerente').save()

# Editar

# t = Type.objects.get(id=1)
# t.name = 'Acionista Diretor'
# t.save()
# print(t)

# Controlando Erros

# try:
#     t = Type.objects.get(pk=2)
#     t.name = 'Presidente'
#     t.save()
# except Exception as e:
#     print(e)

# Deletar

# t = Type.objects.get(pk=2)
# t.delete()

# Outras Formas de Listar

# obj = Type.objects.filter(name__contains='alfredo')
# obj = Type.objects.filter(name__startswith='p')
# obj = Type.objects.filter(name__endswith='or')
# obj = Type.objects.filter(name__in=['Diretor', 'Gestor']).count()
# obj = Type.objects.filter(name__contains='e').exclude(id=9)

# obj = Employee.objects.filter(type_id='1')
#
# for i in Type.objects.filter(name__contains='e').exclude(id=9):
#     print(i.name)


