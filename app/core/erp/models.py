from django.db import models
from datetime import datetime


# Create your models here.

class Type(models.Model):
    name = models.CharField(max_length=150, verbose_name='Nome')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Tipo'
        verbose_name_plural = 'Tipos'
        ordering = ['id']


class Category(models.Model):
    name = models.CharField(max_length=150, verbose_name='Nome', unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'
        ordering = ['id']


class Employee(models.Model):
    category = models.ManyToManyField(Category) # Relacionamento Muito para Muitos
    type = models.ForeignKey(Type, on_delete=models.CASCADE) # Relacionamento 1 para 1 /  Se Deletarmos um 'type' iremos remover em cascata seus 'Employee' relacionados
    names = models.CharField(max_length=150, verbose_name='Nomes')
    dni = models.CharField(max_length=10, unique=True, verbose_name='DNI')
    date_joined = models.DateField(default=datetime.now, verbose_name='Data de Registro')
    date_create = models.DateTimeField(auto_now=True)
    date_update = models.DateTimeField(auto_now_add=True)
    age = models.PositiveIntegerField(default=0)
    salary = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    state = models.BooleanField(default=True)
    avatar = models.ImageField(upload_to='avatar/%y/%m/%d', null=True, blank=True)
    cvitae = models.ImageField(upload_to='avatar/%y/%m/%d', null=True, blank=True)

    # Define qual atributo vai representar o objeto Employee.
    def __str__(self):
        return self.names


class Meta:
    verbose_name = 'Empleado'
    verbose_name_plural = 'Empleados'
    ordering = ['id']
